# Sphinx reports

Build site documentation with [Sphinx](http://sphinx-doc.org/) powered by [Jython](http://www.jython.org/).

I aspire to make a more reactive project and easyer to use than [sphinx-maven](https://github.com/tomdz/sphinx-maven).